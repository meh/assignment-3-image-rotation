#include <from_bmp.h>
#include <stdint.h>
#include <stdio.h>
#include <utility.h>

enum read_status from_bmp(FILE *in, struct image *img) {
  struct bmp_header bmp_header;
  if (fread(&bmp_header, sizeof(struct bmp_header), 1, in) != 1) {
    return READ_IS_NOT_AVAILABLE;
  }
  img->width = bmp_header.biWidth;
  img->height = bmp_header.biHeight;
  img->data = malloc(sizeof(struct pixel) * img->width * img->height);
  if (!img->data) {
    fprintf(stderr, "Error while memory allocation for reading input file\n");
    return READ_INVALID_ALLOCATION;
  }

  fseek(in, bmp_header.bOffBits, SEEK_SET);
  uint8_t padding = get_padding(bmp_header.biWidth);

  for (uint64_t i = 0; i < bmp_header.biHeight; i++) {
    if (fread(img->data + i * bmp_header.biWidth, sizeof(struct pixel),
              bmp_header.biWidth, in) != bmp_header.biWidth) {
      fprintf(stderr, "Error occupped while reading bmp file!\n");
      free(img->data);
      return READ_IS_NOT_AVAILABLE;
    }
    if (fseek(in, padding, SEEK_CUR) != 0) {
      fprintf(stderr, "Error occupped while reading bmp file!\n");
      free(img->data);
      return READ_IS_NOT_AVAILABLE;
    }
  }
  return READ_OK;
}
