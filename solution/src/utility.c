#include <stdint.h>
#include <utility.h>

#define BMP_BF_TYPE 0x4D42
#define BMP_BF_RESERVED 0
#define BMP_BI_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BI_BITCOUNT 24
#define BMP_BI_COMPRESSION 0
#define BMP_BI_XPEELSPERMETER 2834
#define BMP_BI_YPEELSPERMETER 2834
#define BMP_BI_CLRUSED 0
#define BMP_BI_CLRIMPORTANT 0

uint8_t get_padding(uint32_t width) {
  if (width % 4 == 0) {
    return 0;
  }
  return 4 - width * sizeof(struct pixel) % 4;
}

struct bmp_header create_header(const struct image *img) {
  uint32_t offset = sizeof(struct bmp_header);
  uint32_t file_size = img->height * (get_padding(img->width) +
                                      img->width * sizeof(struct pixel));
  uint32_t image_size = file_size + offset;

  return (struct bmp_header){.bfType = BMP_BF_TYPE,
                             .bfileSize = file_size,
                             .bfReserved = BMP_BF_RESERVED,
                             .bOffBits = offset,
                             .biSize = BMP_BI_SIZE,
                             .biWidth = img->width,
                             .biHeight = img->height,
                             .biPlanes = BMP_BI_PLANES,
                             .biBitCount = BMP_BI_BITCOUNT,
                             .biCompression = BMP_BI_COMPRESSION,
                             .biSizeImage = image_size,
                             .biXPelsPerMeter = BMP_BI_XPEELSPERMETER,
                             .biYPelsPerMeter = BMP_BI_YPEELSPERMETER,
                             .biClrUsed = BMP_BI_CLRUSED,
                             .biClrImportant = BMP_BI_CLRIMPORTANT};
}
